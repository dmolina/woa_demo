# Demo WOA

Demo del WOA para la exposición del parque de las ciencias.

Es un código en Julia usando el paquete
[Metaheuristics.jl](https://github.com/jmejia8/Metaheuristics.jl).

Dado que no se puede ejecutar en modo de *solo lectura* en Windows (usando
[PlutoSliderServer](https://github.com/JuliaPluto/PlutoSliderServer.jl)) se optó
por usar una extensión de `Firefox` que ocultaba los distintos elementos editables
(botones y demás).

Los ficheros son:

- Project.toml: Listado de dependencias del proyecto.
- woa.jl: Demo en formato Pluto.
- woa_completo.jl: Versión más completa pero que se consideró excesivamente
  compleja para divulgación.
- edit.sh: *Script* en bash que ejecuta **woa.jl** desde Pluto, para editarlo.
- run.sh: *Script* en bash que ejecuta **woa.jl** desde *PlutoSliderServer*,
  para verlo sin editar (no funciona en Windows).
