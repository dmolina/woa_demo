### A Pluto.jl notebook ###
# v0.19.9

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ 5525b401-bb42-4c11-b88a-c39c868dfc1c
begin
	using Pkg
	Pkg.activate(".")
	# using ShortCodes
	using PlutoUI
	using Plots
	using Metaheuristics
	using PlotThemes
	using Printf
end

# ╔═╡ 2fc4e198-3b58-11ed-2e96-b3fed37c841d
md"""
# Redes de burbujas para pescar SPAM
"""

# ╔═╡ 142e1a6e-3e04-4f06-ad03-274e1c8f2e23
begin
  function ackley(x,y)
  e = exp(1)
  -20*exp(-0.2*sqrt(0.5*(x^2+y^2)))-exp(0.5*(cos(2pi*x)+cos(2pi*y)))+e+20
  end
  function himmel(x,y)
    (x^2+y-11)^2+(x+y^2-7)^2
  end
  function three(x,y)
    2x^2-1.05x^4+x^6/6+x*y+y^2
  end
  function beale(x,y)
    (1.5-x+x*y)^2+(2.25-x+x*y^2)^2+(2.625-x+x*y^3)^2
  end
  function booth(x,y)
    (x-2y-7)^2+(2x+y-5)^2
  end
  function goldstein(x,y)
    (1+(x+y+1)^2*(19-14x+3x^2-14y+6x*y+3y^2))*(30+(2x-3y)^2*(18-32x+12x^2+48y-36x*y+27y^2))
  end
  function rosenbrock(x,y)
    100*(y-x^2)^2+(1-x)^2
  end
   LocalResource(joinpath(pwd(), "logofullweb.png"), :width => 800)
end

# ╔═╡ 65eed846-6f96-4223-aa4a-9d662803923a
@bind select Select([(ackley,4,100) => "Escenario 1", (booth, 10,300) => "Escenario 2", (rosenbrock, 3,300) => "Escenario 3"])

# ╔═╡ 36b5e33c-62a5-4ac3-822e-6406e4bd45d6
md"Número de Muestras: $(@bind N NumberField(10:5:60, default=15))"

# ╔═╡ be53d4e1-7e7d-4477-94cb-9002b18a4205
begin 
	fun,tam,maxsize = select
	range = LinRange(-tam, tam, 100)
	nothing
	# bounds
bounds = [-5ones(2) 5ones(2)]'

# Common options
options = Options(seed=42, store_convergence = true)

function funopt(xs)
	(x,y) = xs
	fun(x,y)
end

# Optimizing
result = optimize(funopt, bounds, WOA(N=N, options=options))

# Devuelvo parámetros
	md"Avance: $(@bind itera Slider(1:maxsize, show_value=false))"
end

# ╔═╡ ca7c5c5d-15c6-4061-b3df-51d803b26f98
begin
	theme(:vibrant)
	# theme(:wong2)
	limit = [first(range),last(range)]
	p2 = contour(range, range, fun, fill=true, levels=50, cgrad=:thermal, size=(800,600))

	if (itera > 0)
		X = positions(result.convergence[itera])
		scatter!(X[:,1], X[:,2], color="white", legend=false, xlimit=limit,ylimit=limit)
	end
	p2
end

# ╔═╡ 4f1ad1e3-c29e-48b4-9548-36d0cab2d2a7
md"""
Imaginemos que queremos poder localizar de entre correos que nos lleguen aquellos que sean SPAM. 

Visualmente vamos a mostrar de color azul los emails que son SPAM, y de otro color aquellos con menor probabilidad de serlo.

El método va a ir generando propuestas (puntos), y conforme más tiempo se ejecuta mejores resultados ofrece.
"""

# ╔═╡ c3f90318-5173-4136-a208-1b49d918ba80
html"""
<style>
	main {
		margin: 0 auto;
		max-width: 2000px;
    	padding-left: max(160px, 10%);
    	padding-right: max(160px, 10%);
	}
</style>
"""

# ╔═╡ be51e402-a9c3-46e0-b47d-c1e75822f3ad
begin
	mean(x)=sum(x)/length(x)
	media = []
   popant = positions.(result.convergence[1:itera])
   for i in 1:length(popant)
	   current = popant[i]
	   push!(media, mean(fun.(current[:,1],current[:,2])))
   end
end

# ╔═╡ Cell order:
# ╟─5525b401-bb42-4c11-b88a-c39c868dfc1c
# ╟─2fc4e198-3b58-11ed-2e96-b3fed37c841d
# ╠═142e1a6e-3e04-4f06-ad03-274e1c8f2e23
# ╟─65eed846-6f96-4223-aa4a-9d662803923a
# ╟─be53d4e1-7e7d-4477-94cb-9002b18a4205
# ╟─36b5e33c-62a5-4ac3-822e-6406e4bd45d6
# ╟─ca7c5c5d-15c6-4061-b3df-51d803b26f98
# ╟─4f1ad1e3-c29e-48b4-9548-36d0cab2d2a7
# ╟─c3f90318-5173-4136-a208-1b49d918ba80
# ╟─be51e402-a9c3-46e0-b47d-c1e75822f3ad
