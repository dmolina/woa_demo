### A Pluto.jl notebook ###
# v0.19.9

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ 5525b401-bb42-4c11-b88a-c39c868dfc1c
begin
	using Pkg
	Pkg.activate(".")
	# using ShortCodes
	using PlutoUI
	using Plots
	using Metaheuristics
	using PlotThemes
	using Printf
end

# ╔═╡ 2fc4e198-3b58-11ed-2e96-b3fed37c841d
md"""
# Redes de burbujas para pescar SPAM (correo no deseado)

Esta página es una demo para mostrar cómo la técnica de caza de las ballenas jorobadas puede usarse para optimizar problemas de ingeniería.
"""

# ╔═╡ 79987f27-f179-4cf2-aed0-a40b8563a0a5
md"""
Imaginemos que queremos poder localizar de entre correos que nos lleguen aquellos que sean SPAM. 

Visualmente vamos a mostrar de color azul los emails que son SPAM, y de otro color aquellos con menor probabilidad de serlo.

El método va a ir generando propuestas (puntos), y conforme más tiempo se ejecuta mejores resultados ofrece.
"""

# ╔═╡ 0649e1d8-3c99-4b41-bbf6-3033640ee2f2
begin 
	function ackley(x,y)
	e = exp(1)
	-20*exp(-0.2*sqrt(0.5*(x^2+y^2)))-exp(0.5*(cos(2pi*x)+cos(2pi*y)))+e+20
	end
	function himmel(x,y)
		(x^2+y-11)^2+(x+y^2-7)^2
	end
	function three(x,y)
		2x^2-1.05x^4+x^6/6+x*y+y^2
	end
	function beale(x,y)
		(1.5-x+x*y)^2+(2.25-x+x*y^2)^2+(2.625-x+x*y^3)^2
	end
	function booth(x,y)
		(x-2y-7)^2+(2x+y-5)^2
	end
	function goldstein(x,y)
		(1+(x+y+1)^2*(19-14x+3x^2-14y+6x*y+3y^2))*(30+(2x-3y)^2*(18-32x+12x^2+48y-36x*y+27y^2))
	end
	function rosenbrock(x,y)
		100*(y-x^2)^2+(1-x)^2
	end
	nothing
end

# ╔═╡ cba2fe4e-aa1b-4b5c-92c2-ab045d51605a
# @bind select Select([(ackley,4,100) => "Escenario 1", (himmel, 6,300) => "Escenario 2", (booth, 10,300) => "Escenario 3", (goldstein, 2,300) => "Escenario 4", (rosenbrock, 3,300) => "Escenario 5"])
@bind select Select([(ackley,4,100) => "Escenario 1", (booth, 10,300) => "Escenario 2", (rosenbrock, 3,300) => "Escenario 3"])

# ╔═╡ 36b5e33c-62a5-4ac3-822e-6406e4bd45d6
md"Número de Muestras: $(@bind N NumberField(10:5:60, default=15))"

# ╔═╡ be53d4e1-7e7d-4477-94cb-9002b18a4205
begin 
	fun,tam,maxsize = select
	range = LinRange(-tam, tam, 100)
	nothing
	# bounds
bounds = [-5ones(2) 5ones(2)]'

# Common options
options = Options(seed=42, store_convergence = true)

function funopt(xs)
	(x,y) = xs
	fun(x,y)
end

# Optimizing
result = optimize(funopt, bounds, WOA(N=N, options=options))

# Devuelvo parámetros
	md"Avance: $(@bind itera Slider(1:maxsize, default=0, show_value=true))"
end

# ╔═╡ ca7c5c5d-15c6-4061-b3df-51d803b26f98
begin
	theme(:vibrant)
	# theme(:wong2)
	limit = [first(range),last(range)]
	p2 = contour(range, range, fun, fill=true, levels=50, cgrad=:thermal)

	if (itera > 0)
		X = positions(result.convergence[itera])
		scatter!(X[:,1], X[:,2], color="white", legend=false, xlimit=limit,ylimit=limit)
	end
	p2
end

# ╔═╡ 79a5818a-ce96-474a-bc1e-20b63f2607f7
fun.(X[:,1],X[:,1])

# ╔═╡ c3f90318-5173-4136-a208-1b49d918ba80
html"""
<style>
	main {
		margin: 0 auto;
		max-width: 1200px;
    	padding-left: max(160px, 10%);
    	padding-right: max(160px, 10%);
	}
</style>
"""

# ╔═╡ be51e402-a9c3-46e0-b47d-c1e75822f3ad
begin
	mean(x)=sum(x)/length(x)
	media = []
   popant = positions.(result.convergence[1:itera])
   for i in 1:length(popant)
	   current = popant[i]
	   push!(media, mean(fun.(current[:,1],current[:,2])))
   end
end

# ╔═╡ c5ba94e9-8640-4661-8dcc-82c37b391388
begin
	f_calls, best_f_value = convergence(result)
    # convergence
    #plot(1:length(best_f_value), best_f_value, label=false, xlabel="Generation", ylabel="fitness", title="Gen: $itera")
    pconv = plot(N.*(1:itera), best_f_value[1:itera], lw=3, xlabel="Número de soluciones", ylabel="Menor Error Encontrado (Log)", yaxis=log, label="Mejor")
	values = positions.(result.convergence[1:itera])
	plot!(N.*(1:itera), media, lw=3, label="Media soluciones")
	plot!(yformatter=:scientific)
	plot!(xformatter = x -> x |> round |> Int32, yformatter = y->y)
end

# ╔═╡ af8314dc-fab9-4f67-990a-504f75f3fc08
if itera == 1
	plot(p2, size=(380,500))
else
plot(p2, pconv, size=(1000,600))
end

# ╔═╡ ed3e7a39-f13d-4382-a1ac-724fd4a5b8d0
plot(p2, pconv, size=(1000,600))

# ╔═╡ Cell order:
# ╟─5525b401-bb42-4c11-b88a-c39c868dfc1c
# ╟─2fc4e198-3b58-11ed-2e96-b3fed37c841d
# ╟─79987f27-f179-4cf2-aed0-a40b8563a0a5
# ╟─0649e1d8-3c99-4b41-bbf6-3033640ee2f2
# ╟─cba2fe4e-aa1b-4b5c-92c2-ab045d51605a
# ╟─be53d4e1-7e7d-4477-94cb-9002b18a4205
# ╟─36b5e33c-62a5-4ac3-822e-6406e4bd45d6
# ╟─af8314dc-fab9-4f67-990a-504f75f3fc08
# ╟─ca7c5c5d-15c6-4061-b3df-51d803b26f98
# ╟─79a5818a-ce96-474a-bc1e-20b63f2607f7
# ╟─c5ba94e9-8640-4661-8dcc-82c37b391388
# ╟─c3f90318-5173-4136-a208-1b49d918ba80
# ╟─ed3e7a39-f13d-4382-a1ac-724fd4a5b8d0
# ╟─be51e402-a9c3-46e0-b47d-c1e75822f3ad
